#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Admin Hide"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Admin Hide"
#define PLUGIN_URL "zipcore.net"

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

#include <smlib>
#include <multicolors>

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("adminhide");
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	HookEvent("player_disconnect", Event_Block, EventHookMode_Pre);
	HookEvent("player_connect", Event_Block, EventHookMode_Pre);
	HookEvent("player_team", Event_Block, EventHookMode_Pre);
}

public Action Event_Block(Handle event, const char[] name, bool dontBroadcast)
{
	return Plugin_Handled;
}

public void OnConfigsExecuted()
{
	SDKHookEx(GetPlayerResourceEntity(), SDKHook_ThinkPost, OnResourceThink);
}

public void OnResourceThink(int iEntity)
{
	static int m_bConnected = -1;
	if (m_bConnected == -1)
		m_bConnected = FindSendPropInfo("CCSPlayerResource", "m_bConnected");
	
	LoopIngamePlayers(i)
	{
		if(GetClientTeam(i) > 1)
			continue;
		
		if(!Client_HasAdminFlags(i, ADMFLAG_GENERIC) && !Client_HasAdminFlags(i, ADMFLAG_ROOT))
			continue;
		
		SetEntData(iEntity, m_bConnected + (i * 4), false, true, true);
	}
}